#!/usr/bin/python

import telebot
from yahoo_finance import Share
import json
import re
import wget
import os

bot = telebot.TeleBot("234544948:AAEgYmLbbTcdoCP5m-tw-bhZFVx-jsx40B4")

@bot.message_handler(commands=['help'])
def send_help(message):
    bot.reply_to(message, "Available commands:" + 
                          "\nget_stock_graph - sends you the stock's graph of the day" +
                          "\nget_open -  info about stock open" +
                          "\nget_price -  info about stock price" +
                          "\nrefresh_data - updated info about stock price, open & tradetime" +
                          "\nget_historical - historical stock info eg: '/get_historical GOOGL yyyy-mm-dd yyyy-mm-dd'" +
                          "\nget_change - changes in stock" +
                          "\nget_prev_close - info on previous closing" +
                          "\nget_avg_daily_volume - average on daily volume" +
                          "\nget_stock_exchange - info on stock exchange" +
                          "\nget_market_cap - info on market cap" +
                          "\nget_book_value - info on book value" +
                          "\nget_ebitda - info on Earnings before interest, tax, depreciation and amortization" +
                          "\nget_days_high - the stock highest value of the day" +
                          "\nget_days_low - the stock lowest value of the day" +
                          "\nget_year_high - the stock highest value of the year" +
                          "\nget_year_low - the stock lowest value of the year" +
                          "\nget_50day_moving_avg - average on 50 day" +
                          "\nget_200day_moving_avg - average on 200 day" +
                          "\nget_price_earnings_ratio - stock price earning ratio" +
                          "\nget_price_earnings_growth_ratio - stock price earning growth ratio" +
                          "\nget_price_book - the stock price book" +
                          "\nget_short_ratio - the stock short ratio")

@bot.message_handler(commands=['start'])
def send_help(message):
    bot.reply_to(message, "This bot can retrieve any information about stock exchange. Informations are gathered from yahoo finance.\nType /help for list of commands\n#### develop by imaprog, any comments email me at shafiqaziz06@gmail.com ####")

#@bot.message_handler(commands=['check'])
#def check_stock(message):
#    bot.reply_to(message, ystockquote.get_price_book('FTSB'))

@bot.message_handler(commands=['get_open'])
def stock_open(message):
    price_book_length = len(message.text)
    if price_book_length <= 9:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[9:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        bot.reply_to(message, prep.get_open())
      else:
        bot.reply_to(message, "Sorry, invalid Format!")

@bot.message_handler(commands=['get_price'])
def stock_get_price(message):
    bid_realtime_length = len(message.text)
    if bid_realtime_length <= 10:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[10:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        bot.reply_to(message, prep.get_price())
      else:
        bot.reply_to(message, "Sorry, invalid Format!")

@bot.message_handler(commands=['refresh_data'])
def stock_refresh(message):
    refresh_length = len(message.text)
    if refresh_length <= 13:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[13:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        prep.refresh()
        bot.reply_to(message, "Stock Price: %s \nStock Open: %s \nStock Trade Time: %s" % (prep.get_price(), prep.get_open(), prep.get_trade_datetime()) )
      else:
        bot.reply_to(message, "Sorry, invalid Format!")

@bot.message_handler(commands=['get_historical'])
def stock_historical(message):
    refresh_length = len(message.text)
    if refresh_length <= 15:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[15:-22:]
      start_date = message.text[-21:-11]
      end_date = message.text[-10:]

      match_start_date=re.search(r'(\d+-\d+-\d+)',start_date) 
      match_end_date=re.search(r'(\d+-\d+-\d+)',end_date)
      if not match_end_date or not match_start_date:

        bot.reply_to(message, "Sorry, missing dates or invalid format!")
      else:
        prep = Share("%s" % stock)
        result = prep.get_historical("%s" % start_date, "%s" % end_date)

        Adj_Close = result[0]['Adj_Close']
        Low = result[0]['Low']
        Open = result[0]['Open']
        Date = result[0]['Date']
        Close = result[0]['Close']
        Volume = result[0]['Volume']
        High = result[0]['High']

        bot.reply_to(message, "Adjusted Closing: %s \nLow: %s \nOpen: %s \nDate: %s \nClose: %s \nVolume: %s \nHigh: %s" % (Adj_Close, Low, Open, Date, Close, Volume, High) )

@bot.message_handler(commands=['get_change'])
def stock_get_change(message):
    get_change_length = len(message.text)
    if get_change_length <= 11:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[11:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        bot.reply_to(message, prep.get_change())
      else:
        bot.reply_to(message, "Sorry, invalid Format!")


@bot.message_handler(commands=['get_prev_close'])
def stock_get_close(message):
    get_close_length = len(message.text)
    if get_close_length <= 15:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[15:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        bot.reply_to(message, prep.get_prev_close())
      else:
        bot.reply_to(message, "Sorry, invalid Format!")
        

@bot.message_handler(commands=['get_avg_daily_volume'])
def stock_get_avg(message):
    get_avg_length = len(message.text)
    if get_avg_length <= 21:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[21:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        bot.reply_to(message, prep.get_avg_daily_volume())
      else:
        bot.reply_to(message, "Sorry, invalid Format!")

@bot.message_handler(commands=['get_stock_exchange'])
def stock_se_avg(message):
    get_se_length = len(message.text)
    if get_se_length <= 19:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[19:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        bot.reply_to(message, prep.get_stock_exchange())
      else:
        bot.reply_to(message, "Sorry, invalid Format!") 

@bot.message_handler(commands=['get_market_cap'])
def stock_market_cap(message):
    get_mc_length = len(message.text)
    if get_mc_length <= 15:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[15:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        bot.reply_to(message, prep.get_market_cap())
      else:
        bot.reply_to(message, "Sorry, invalid Format!")


@bot.message_handler(commands=['get_book_value'])
def stock_book_value(message):
    get_bv_length = len(message.text)
    if get_bv_length <= 15:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[15:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        bot.reply_to(message, prep.get_book_value())
      else:
        bot.reply_to(message, "Sorry, invalid Format!")


@bot.message_handler(commands=['get_ebitda'])
def stock_ebitda(message):
    get_ebi_length = len(message.text)
    if get_ebi_length <= 11:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[11:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        bot.reply_to(message, prep.get_ebitda())
      else:
        bot.reply_to(message, "Sorry, invalid Format!") 

    
@bot.message_handler(commands=['get_days_high'])
def stock_days_high(message):
    get_dayhigh_length = len(message.text)
    if get_dayhigh_length <= 14:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[14:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        bot.reply_to(message, prep.get_days_high())
      else:
        bot.reply_to(message, "Sorry, invalid Format!")


@bot.message_handler(commands=['get_days_low'])
def stock_days_low(message):
    get_daylow_length = len(message.text)
    if get_daylow_length <= 13:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[13:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        bot.reply_to(message, prep.get_days_low())
      else:
        bot.reply_to(message, "Sorry, invalid Format!")


@bot.message_handler(commands=['get_year_high'])
def stock_year_high(message):
    get_yearhigh_length = len(message.text)
    if get_yearhigh_length <= 14:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[14:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        bot.reply_to(message, prep.get_year_high())
      else:
        bot.reply_to(message, "Sorry, invalid Format!")


@bot.message_handler(commands=['get_year_low'])
def stock_year_low(message):
    get_yearlow_length = len(message.text)
    if get_yearlow_length <= 13:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[13:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        bot.reply_to(message, prep.get_year_low())
      else:
        bot.reply_to(message, "Sorry, invalid Format!")


@bot.message_handler(commands=['get_50day_moving_avg'])
def stock_fiftydayavg_low(message):
    get_fiftydayavg_length = len(message.text)
    if get_fiftydayavg_length <= 21:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[21:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        bot.reply_to(message, prep.get_50day_moving_avg())
      else:
        bot.reply_to(message, "Sorry, invalid Format!")


@bot.message_handler(commands=['get_200day_moving_avg'])
def stock_twohunddayavg_low(message):
    get_twohunddayavg_length = len(message.text)
    if get_twohunddayavg_length <= 22:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[22:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        bot.reply_to(message, prep.get_200day_moving_avg())
      else:
        bot.reply_to(message, "Sorry, invalid Format!")


@bot.message_handler(commands=['get_price_earnings_ratio'])
def stock_er_low(message):
    get_er_length = len(message.text)
    if get_er_length <= 25:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[25:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        bot.reply_to(message, prep.get_price_earnings_ratio())
      else:
        bot.reply_to(message, "Sorry, invalid Format!")


@bot.message_handler(commands=['get_price_earnings_growth_ratio'])
def stock_egr_low(message):
    get_egr_length = len(message.text)
    if get_egr_length <= 32:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[32:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        bot.reply_to(message, prep.get_price_earnings_growth_ratio())
      else:
        bot.reply_to(message, "Sorry, invalid Format!")


@bot.message_handler(commands=['get_price_sales'])
def stock_psale_low(message):
    get_psale_length = len(message.text)
    if get_psale_length <= 16:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[16:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        bot.reply_to(message, prep.get_price_sales())
      else:
        bot.reply_to(message, "Sorry, invalid Format!")


@bot.message_handler(commands=['get_price_book'])
def stock_pbook_low(message):
    get_pbook_length = len(message.text)
    if get_pbook_length <= 15:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[15:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        bot.reply_to(message, prep.get_price_book())
      else:
        bot.reply_to(message, "Sorry, invalid Format!")


@bot.message_handler(commands=['get_short_ratio'])
def stock_sratio_low(message):
    get_sratio_length = len(message.text)
    if get_sratio_length <= 16:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[16:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = Share("%s" % stock)
        bot.reply_to(message, prep.get_short_ratio())
      else:
        bot.reply_to(message, "Sorry, invalid Format!")


@bot.message_handler(commands=['get_stock_graph'])
def send_photo(message):
    get_sg_length = len(message.text)
    if get_sg_length <= 16:
      bot.reply_to(message, "Sorry, missing arguments!")
    else:
      stock = message.text[17:]
      invalid_format = re.search(r"(.+\w+\s+.+\w)", stock)
      if not invalid_format:
        prep = "%s" % stock
        name = "%s.png" % prep
        command = 'wget "http://chart.finance.yahoo.com/t?s='+stock+'&lang=en-US&region=US&width=600&height=400" -O '+name+' '
        os.system(command)
        img = open('%s' % name, 'rb')
        bot.send_photo(message.chat.id, img, reply_to_message_id=message.message_id)
        img.close()
        os.remove('%s' % name)
      else:
        bot.reply_to(message, "Sorry, invalid Format!")


@bot.message_handler(func=lambda message: True)
def echo_all(message):
    bot.reply_to(message, "Sorry, wrong command!")

bot.polling(none_stop=True)

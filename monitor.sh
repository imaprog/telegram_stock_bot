#!/bin/bash
until python stock.py; do
    echo "'stock.py' crashed with exit code $?. Restarting..." >&2
    sleep 1
done
